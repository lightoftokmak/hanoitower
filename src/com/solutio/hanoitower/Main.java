package com.solutio.hanoitower;

import com.solutio.hanoitower.model.Tower;
import com.solutio.hanoitower.model.Type;
import com.solutio.hanoitower.service.HanoiTowers;

public class Main {

    private static void hanoiIterative(final int n) {
        HanoiTowers hanoiTowers = new HanoiTowers(n,true);
        hanoiTowers.solve();
        Tower destinationTower = hanoiTowers.getByType(Type.Destination);

        /*
         *Print Result
         */
        System.out.println("------------------------------------");
        System.out.println("Destination Tower Height:" + n + ", Number of iterations:" + hanoiTowers.getNumberOfIteration());
        while (!destinationTower.isTowerEmpty()) {
            System.out.println("Disc:" + destinationTower.pullDisc().getValue());
        }
        System.out.println("------------------------------------");
    }

    public static void main(String[] args) {

        hanoiIterative(5);

    }

}

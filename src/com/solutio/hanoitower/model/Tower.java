package com.solutio.hanoitower.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Tower {

    /*
     *Ignores bigger values than previous ones
     */
    private Stack<Disc> discStack = new Stack<>();
    private Type type;

    public Tower(Type type) {
        this.type = type;
    }


    /*
     *Push disc to tower if tower is empty or disc is not bigger.
     */
    public boolean pushDisc(Disc disc) {
        if (disc == null) return false;

        if (discStack.isEmpty()) {
            // System.out.println("Disc "+disc.getValue()+" is added to "+type);
            discStack.push(disc);
            return true;
        }

        Disc peek = discStack.peek();

        if (peek.getValue() > disc.getValue()) {
            //   System.out.println("Disc "+disc.getValue()+" is added to "+type);
            discStack.push(disc);
            return true;
        }

        return false;
    }

    public Disc pullDisc() {
        return discStack.pop();
    }

    public boolean isTowerEmpty() {
        return discStack.isEmpty();
    }

    public Disc head() {
        return !discStack.isEmpty() ? discStack.peek() : null;
    }

    /*
     *Only for debugging this code block is not efficient.
     * Not use if your are not debugging
     */
    public void snapShot() {

        List<Disc> cloneStack = new ArrayList<>((Stack<Disc>) discStack.clone());
        System.out.print(type+":|");
        for(Disc d:cloneStack){
            System.out.print(d.getValue() + "|");
        }
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }
}

package com.solutio.hanoitower.model;

public enum Type {
    Source("Tower 1"),Buffer("Tower 2"),Destination("Tower 3");
    private final String name;
    Type(String name){
        this.name = name;
    }
    public String toString() {
        return this.name;
    }
}

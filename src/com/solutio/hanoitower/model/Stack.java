package com.solutio.hanoitower.model;

import java.util.ArrayList;
import java.util.List;

/*
 *This stack implementation is not used.
 *Just for example.
 */
public class Stack<E> {

    private List<E> stack = new ArrayList<>();
    private int head = -1;

    public void push(E item) {
        head++;
        stack.add(item);
    }

    public E pop() {

        if(isEmpty()) return null;

        E item = stack.get(head);
        stack.remove(head);
        head--;
        return item;
    }

    public E peek() {
        return stack.get(head);
    }
    public boolean isEmpty() {
        return (head == -1)? true:false;
    }
}

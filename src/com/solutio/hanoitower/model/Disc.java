package com.solutio.hanoitower.model;

import java.util.Objects;

public class Disc {

    private int value;

    public Disc(Integer value) {

        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disc disc = (Disc) o;
        return value == disc.value;
    }

    @Override
    public int hashCode() {

        return Objects.hash(value);
    }
}

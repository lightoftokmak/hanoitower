# HanoiTower

The Tower of Hanoi (also called the Tower of Brahma or Lucas' Tower and sometimes 
pluralized) is a mathematical game or puzzle. The objective of the puzzle is to move the entire stack to another rod, obeying the following 
simple rules

*  Only one disk can be moved at a time.
*  Each move consists of taking the upper disk from one of the stacks and placing it on top of another stack i.e. a disk can only be moved if it is the uppermost disk on a stack.
*  No disk may be placed on top of a smaller disk.

With 3 disks, the puzzle can be solved in 7 moves. The minimal number of moves required to 
solve a Tower of Hanoi puzzle is 2n − 1, where n is the number of disks.
